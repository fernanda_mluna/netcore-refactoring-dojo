﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {

            IList<Item> Ingressos(IList<Item> lista)
            {
                foreach (var item in lista) 
                {
                    if (item.Nome == "Ingressos para o concerto do TAFKAL80ETC")
                    {
                        if (item.Qualidade < 50)
                        {
                            item.Qualidade += 1;

                            if (item.PrazoValidade < 11 && item.Qualidade < 50) 
                            {
                                item.Qualidade += 1;
                            }
                            if (item.PrazoValidade < 6 && item.Qualidade < 50)
                            {
                                item.Qualidade += 1;
                            }
             
                        }

                        if (item.PrazoValidade <= 0)
                        {
                            item.Qualidade -= item.Qualidade;
                        }
                        item.PrazoValidade -= 1;
                    }
                }
                return lista;
            }

            IList<Item> QueijoBrieEnvelhecido(IList<Item> lista)
            {
                foreach (var item in lista) 
                {
                    if (item.Nome == "Queijo Brie Envelhecido")
                    {
                        if (item.Qualidade < 50)
                        {
                            item.Qualidade += 1;
                            
                            if (item.PrazoValidade <= 0 && item.Qualidade < 50)
                            {
                                item.Qualidade += 1;
                            }                                         
                        }
                        item.PrazoValidade -= 1;
                    }
                }
                return lista;
            }

            IList<Item> ItensGerais(IList<Item> lista)
            {
                foreach (var item in lista) 
                {
                    if (item.Nome != "Sulfuras, a Mão de Ragnaros" && item.Nome != "Queijo Brie Envelhecido" 
                        && item.Nome != "Ingressos para o concerto do TAFKAL80ETC" && item.Nome.Contains("Conjurado") != true)
                    {
                        if (item.Qualidade > 0)
                        {
                            item.Qualidade -= 1;
                            if (item.PrazoValidade <= 0) 
                            {
                                item.Qualidade -= 1;
                            }
                        }   
                        item.PrazoValidade -= 1;
                    }                    
                }
                return lista;
            }

            IList<Item> Conjurados(IList<Item> lista)
            {
                foreach (var item in lista) 
                {
                    if (item.Nome.Contains("Conjurado"))
                    {
                        if (item.PrazoValidade > 0)
                        {
                            item.Qualidade -= 1;
                        } else {
                            item.Qualidade -= 2;
                            if (item.Qualidade < 0) 
                            {    
                                item.Qualidade = 0;
                            }
                        }
                        item.PrazoValidade -= 1;
                    }                    
                }
                return lista;
            }

            Itens = Ingressos(Itens);
            Itens = QueijoBrieEnvelhecido(Itens);
            Itens = ItensGerais(Itens);
            Itens = Conjurados(Itens);

        }
    }
}

